function sample=takesample(sampling,space,east,north)
% Usage sample=takesample(sampling,space,east,north)

% This function is part of the auvfeature simulation collection

% This function overlays the sampling pattern supplied as an input and
% overlays it on the space grid at a position provided as input, to provide an average value from the space
% under the pattern.

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Get a subset of the space, that aligns with the pattern position.

% Define the spatial extent
north_max=north+sampling.radius;
north_min=north-sampling.radius;
east_min=east-sampling.radius;
east_max=east+sampling.radius;

% Get the space vectors of these extents and pull out a sub-array

selection = space.spacegrid(space.xvector>=east_min&space.xvector<=east_max,space.yvector>=north_min&space.yvector<=north_max);

%% At the moment just using point sample - edit here for complex pattern

sample = selection * sample.pattern;

%% Average the sample

sample = nanmean(sample(:));

%%
sample = selection * 1;