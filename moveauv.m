 function auv=moveauv(waypoints,interval,endurance,speed)
% Usage auv=moveauv(waypoints,sampling,endurance,speed)

% This function is part of the auvfeature simulation collection
% This function takes takes what we know about a vehicle and sends around a track.
% The waypoints (in metre space) define the indented path. The vehicle
% position is determined by its speed. If path exceeds endurance, the
% vehicle returns to the 1st point directly. The location update interval
% is determined by the structure describing the sampling behaviour. The
% path is effectivly a map of where the vehicle is when a sample is taken.
% If this is too infrequent, it may cause problems of the vehicle missing
% waypoints as the turning is updated at sample intervals. It is envisaged
% that the sampling will be frequent and that this generally won't be a
% problem.

% All units refer to metres, e.g. ms^1. Endurance is also in m.

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Setup the collection vectors
% Number of samples capable within the endurance and speed window
samples = ceil((endurance / speed) / interval);

distance = NaN(samples,1);
east = NaN(samples,1);
north = NaN(samples,1);
time = NaN(samples,1);
heading = NaN(samples,1);
circuit = NaN(samples,1);
turninglimit = 130; %Not currently working fully - possibly not necessary - issue where it's only active at contol intervals rather than integrated effect between control updates

targetradius = 5; % Allow tolerance for reaching target

%% Start AUV log
%tic;
%filename=strcat('AUV_sim.log');
%fid = fopen(filename, 'a');


%Print in details
%fprintf(fid,'%s\n', '#########################');
% fprintf(fid,'%s\n', '### New Simulation Run###');
% fprintf(fid,'%s\n', '#########################');
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', datestr(now()));
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', ['Vehicle speed: ' num2str(speed) 'm/s']);
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', ['Vehicle endurance: ' num2str(endurance) 'm']);
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', ['Vehicle Guidance and Control nterval: ' num2str(interval) 'm']);
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', 'Waypoints:');
% for ii=1:length(waypoints)
%    fprintf(fid,'%s\n', num2str(waypoints(ii,:)));
% end
% fprintf(fid,'%s\n', '');
% fprintf(fid,'%s\n', 'Starting NAV log');
% fprintf(fid,'%s\n', '********');
%% Function to calculate range from position to waypoint 
    function [range,bearing] = rangeandbearing(position,target)
        % position and target are in 2x1 vectors
        xoffset = target(1) - position (1);
        yoffset = target(2) - position (2);

        [angle,range]=cart2pol(xoffset,yoffset);
        
        bearing = angle*(180/pi); % goodbye radians
        % Set the angle to between 0 and 360
        bearing(bearing < 0) = bearing + 360;
        
        % Align to compass
        bearing = 90 - bearing;
        bearing (bearing < 0 ) = bearing + 360;
    end 

%% Function to calculate new position given speed (not velocity) and heading
    function [x,y] = advancevehicle(speed,heading,interval)
        % Distance travelled is a function of the speed and update interval
        moverange = speed * interval;
        
        % Convert heading into angle (radians)
        angle = 90 - heading;
        if (angle < 0)
            angle = angle + 360; 
        end
        theta = angle / (180/pi);
        
        
        % Project new x,y vectors
        [x,y]=pol2cart(theta,moverange);
        
    end

%% Function to set the bearing when turning is restricted
 function newbearing = limitturning(heading, bearing, turninglimit);
     %Limit the bearing to the turning radius
        newbearing = bearing; 
     
       % Get the required turn
       deltaturn=abs(heading-bearing);
       deltaturn(deltaturn>180)=abs(deltaturn-360);
       deltaturn(deltaturn<-180)=abs(deltaturn+360);
      
       %limit the bearing from turning limit + heading
       if (deltaturn>turninglimit)
           % Try clockwise and counter
           clockwise = heading+deltaturn;
           clockwise(clockwise>180)=abs(clockwise-360);
           clockwise(clockwise<-180)=abs(clockwise+360);
           
           counterclockwise = heading-deltaturn;
           counterclockwise(counterclockwise>180)=abs(counterclockwise-360);
           counterclockwise(counterclockwise<-180)=abs(counterclockwise+360);
           
           if (clockwise<counterclockwise)
                newbearing = heading+turninglimit;  
           else 
               newbearing = heading-turninglimit;  
           end
       end
end

%% Run the vehicle for each time step
    % Set starting conditions
    distance(1) = 0;
    time(1) = 0;
    east (1) = waypoints(1,1);
    north (1) = waypoints (1,2);
    circuit(1) = 0;
    % Determine starting heading
    [~,heading(1)] = rangeandbearing([east(1),north(1)],waypoints(2,:));
    wp=1; % Set a waypoint target
    
    % Get your iterating hat on...
   % h = waitbar(0,'Calculating AUV positions');
   actualtargetradius=targetradius; % Give the target radius the ability to grow if the vehicle is missing the target repeatedly
   lastrange=NaN;
    for ii=1:samples-1
       time(ii+1) = time(ii)+interval;
       mtime = [num2str(time(ii+1)) 's'];
       circuit(ii+1) = circuit(ii); %Update the circuit
       
       % Look at distance travelled and distance to 1st waypoint. Go home at endurance max.
       [range,~] = rangeandbearing([east(ii),north(ii)],waypoints(1,:));
       if ((distance(ii)+range)>=endurance)
            wp=1;
           %  text=[num2str(endurance-distance(ii)) 'm of endurance remaining.'];    
            %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
            %text='Returning to origin';    
            %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
            %disp('returning');
       end
        
       % Decide next target (if close enough to current target, increment)
       [range,bearing] = rangeandbearing([east(ii),north(ii)],waypoints(wp,:));
       
       if (range>lastrange)
                actualtargetradius=actualtargetradius*1.25;
       end
       
       if (lastrange < actualtargetradius)
            text=['Range to WP' num2str(wp) ': ' num2str(range) 'm'];    
            %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
            
               
            
            range = NaN;
            actualtargetradius=targetradius; %Reset the target radius
           
           %text=['Goto WP' num2str(wp)];
           %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
           
           if (wp == 1)
               circuit(ii+1) = circuit(ii+1)+1; % increment circuit number when sent to WP2
               
               %text = '**New circuit**';
               %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
               
           end
           wp=wp+1;
           wp(wp>length(waypoints))=1; % If reaching the end, go back to beginning
           % Update the range and bearing
           
           [~,bearing] = rangeandbearing([east(ii),north(ii)],waypoints(wp,:));
           
       end
      %text = ['Command bearing ' num2str(bearing,3) ' deg'];
      %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
      
          bearing = limitturning(heading(ii),bearing,turninglimit); % Adjust my bearing, based on the turning limit, to constrain the movement of the vehicle.
      
      % Calculate the vector to the next time step position.
      [x,y] = advancevehicle(speed,bearing,interval);
      
      %text = ['Advance ' num2str(x,4) ' north, ' num2str(y,4) ' east'];
      %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
      distance(ii+1) = distance(ii)+sqrt(x^2+y^2);
      north(ii+1) = north(ii)+y;
      east(ii+1) = east(ii)+x;
      heading(ii+1) = bearing;
     % waitbar(ii/(samples-1),h);
     lastrange = range;
    end
    
    %% Get final state
    [range,~] = rangeandbearing([east(ii),north(ii)],waypoints(1,:));
    %text = ['Vehicle path finished ' num2str(range,2) ' m from deployment'];
      %fprintf(fid,'%s\n', [num2str(toc) '-' mtime '-' text]);
    
      
    %close (h);
%% Finish - write out to auv structure
auv = struct(...
    'time',time,...
    'interval',interval,...
    'distance',distance,...
    'north',north,...
    'east',east,...
    'heading',heading,...
    'circuit',circuit...
    );

 %% Close the file
 
 
%fclose (fid);


end