% This function is part of the auvfeature simulation package
% This is a recipe for use by Gary in simulating the collection of data by
% an AUV.

% Damien Guihen, UTAS, March 2017 (damien.guihen@utas.edu.au)

%% Setup the collection vectors
% Setup scenario
replicates = 2;
targetsizerange = [10,20,30];
targetnumberrange = [10,20];

iterations = replicates * length(targetsizerange) * length(targetnumberrange);
disp(['Running ' num2str(iterations) ' iterations of the AUV sampling'])

% Generate the input vectors
targetnumlist = repmat(targetnumberrange,replicates,1);
targetnumlist = targetnumlist(:);
targetnumlist = repmat(targetnumlist,1,length(targetsizerange));

% Geneate the collection vectors
sizetargets = nan((iterations/length(targetsizerange)),length(targetsizerange));

iterationid = repmat((1:(iterations/length(targetsizerange)))',1,length(targetsizerange));

targetsize = repmat(targetnumberrange',1,length(targetsizerange));
iterationid = repmat((1:replicates)',(iterations/replicates),1);

%% Set the space and target
east = 5000;
north = 1000;
res = 5;

endurance = 8000;
speed = 1.5;
guidance = 1;

%% Define the sampling footprint radius in metres

sampling_footprint = 100;


%% Generate waypoints
  waypoints=[10,0;...
             10,900;...
             100,900;...
             100,10;...
             200,10;...
             200,900;...
             300,900;...
             300,10;...
             400,10;...
             400,900];

%% Run simulation
counter = 0;
for jj=1:length(targetsizerange)
        % I don't like nested for loops, but...
        for ii = 1:(iterations/length(targetsizerange))
        
        counter = counter + 1;
        
        [simspace, retarget] = setup_space_and_target(east,north,res,res,targetsizerange(jj));
   
        numtargets = targetnumlist(ii,jj);
       
        actualtargets(ii,jj) = numtargets;
        sizetargets(ii,jj) = targetsizerange(jj);
    
        %% Run the AUV mission and extract analysis

        % Survey pattern
        [simspace,auv,collection,analysis] = runauvsimulation(simspace,retarget,waypoints,numtargets,endurance,speed,guidance,sampling_footprint);
        disp (['Finished #' num2str(counter)]);
       
        
        % Make some figures
        % Show where the vehicle collected data
        figure;
        scatter(collection.east,collection.north,200,collection.data,'.');
        xlabel ('Metres east');
        ylabel ('Metres north');
        title (['Data collected by the vehicle - Run # ' num2str(counter)] );
        y = colorbar;
        ylabel (y, 'Signal Strength');
        axis equal;
        % Overlap the vehicle's path on the space
        figure;
        pcolor(simspace.xvector,simspace.yvector,simspace.spacegrid');
        shading flat;
        hold
        plot (collection.east,collection.north,'r');
        y = colorbar;
        ylabel (y, 'Signal Strength');
        axis equal;
        xlabel ('Metres east');
        ylabel ('Metres north');
        title (['Actual targets in space - Run # ' num2str(counter)] );

        end
    
end

%% Straighen out all the arrays
sizetargets = sizetargets(:);
iterationid = iterationid(:);

%% Clean up the stray variables
clear counter east numtargets endurance y filename guidance iterations jj north ii progress replicates res retarget speed targetnumberrange targetnumlist targetsize targetsizerange ans