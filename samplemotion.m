function measurement = samplemotion(space,auv,sampling)
%usage measurement = samplemotion(space,auv,sampling)

% This function is part of the auvfeature simulation collection
% Taking space, auv, and sampling strutures the function interpolates the
% position of each measurments and samples the identified space according to its
% sampling pattern, measurement locations.
% The structure output contains the raw measurment of the space.

% Damien Guihen, UTAS, August 2016 (damien.guihen@utas.edu.au)

%% Use the AUV motion and sampling behaviour to get a list of sampling locations
    % Generate timestamps for the instrument
     instrument_time=0:sampling.interval:auv.time(end);
     
     % Interpolate location for each time step
    samplex = interp1(auv.time,auv.east,instrument_time);
    sampley = interp1(auv.time,auv.north,instrument_time);

%% Use the locations and sampling behaviour to generate the appropriate arrays for storing the data
    data = NaN(length(instrument_time),sampling.channels);

%% Define the sample footprint in the simulated space
        % Generate a circular footprint for sensor
        meshsize=25;
        [xx, yy] = meshgrid(1:meshsize);
        footprint = +(sqrt((xx-(meshsize/2)).^2+(yy-(meshsize/2)).^2)<=meshsize/2.00075);

        % Generate appropriate vectors for the footprint
        vec=0:((2*sampling.radius)/(meshsize-1)):(2*sampling.radius); %simple vector for dimensions of circular target
        txvec=vec;
        tyvec=vec';
        
        % Interpolate the pattern for the space
        newvecx=txvec(1):space.xres:txvec(end);
        newvecy=tyvec(1):space.yres:tyvec(end);
        
        footprint=interp2(txvec,tyvec',footprint,newvecx,newvecy');
    
%% For each sample location, cut out the data and write to an array
        centrex=floor(mean(1:length(newvecx)));
        centrey=floor(mean(1:length(newvecy)));

        back=centrex-1;
        forward=length(newvecx)-centrex;
        down=centrey-1;
        up=length(newvecy)-centrey;
        
    for ii = 1:size(data,1);
      
        xxx=space.xvector(space.xvector>=(samplex(ii)-(back*space.xres)) & space.xvector<=(samplex(ii)+(forward*space.xres)));
        yyy=space.yvector(space.yvector>=(sampley(ii)-(down*space.yres)) & space.yvector<=(sampley(ii)+(up*space.yres)));
        
        [~,qx] = intersect(space.xvector,xxx);
        [~,qy] = intersect(space.yvector,yyy);
        
        sample = space.spacegrid(qx,qy)';
        
        % This could do with being shaped - at the moment the selection in
        % rectangular
        
        data(ii) = nanmean(sample(:));
        
    end

%% Save to the measurment structure
    measurement=struct(...
        'time',instrument_time,...
        'east',samplex,...
        'north',sampley,...
        'data',data...
        );

%% Done!
end