function output = analyse_sampling(collection,simspace)
% usage targetfit = targetfit(space,target)

% This function is part of the auvfeature simulation collection
% This component is used to create a recipe for analysis of the output
% sample. 

% Damien Guihen, UTAS, September 2016 (damien.guihen@utas.edu.au)
%% Start analysis of sampling


% convert all values to 1 or 0
collection.data(collection.data>0)=1;

% Grid the collected samples
newfield=griddata(collection.east,collection.north,collection.data,simspace.xvector,simspace.yvector');

% convert all values to 1 or 0
newfield(isnan(newfield)==1)=0;
newfield(newfield<.75)=0;

output.grid = newfield;
[output.boundaries,~,output.numobservedtargets,~]=bwboundaries(im2bw(newfield),'noholes',4);
%[~,~,output.numtargets,~]=bwboundaries(im2bw(simspace.spacegrid),'noholes',4);


end