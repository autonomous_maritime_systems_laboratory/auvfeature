function [simspace,auv,collection,analysis] = runauvsimulation(simspace,retarget,waypoints,numtargets,endurance,speed,guidance,sampling_footprint)

% This function is a wrapper for the auvfeature function set. Use this to
% arrange simulations to answer specific questions.

% Inputs:
%   - simspace      the simulation space
%   - retarget      the regridded target
%   - numtargets    number of targets to position within the space
%   - waypoints     defined route for the AUV
%   - endurance     AUV endurance in m
%   - speed         AUV speed in m
%   - guidance      AUV navigation update frequency in seconds
%   - sampling_footprint *Optional - radius of sensor in metres

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)
    
%% Populate the space with targets
    posattempts=nan(numtargets,1);
   % h=waitbar(0,'Positioning targets');
    for ii=1:length(posattempts)
      [simspace.spacegrid,posattempts(ii)]=targetposition('target',retarget,'space',simspace,'margin',0);
    %  waitbar((ii/length(posattempts)),h);
    end
    %close (h)  
         
  %% Set up the sampling
 % sampling=setupsample(radius,resolution,interval)

 if (exist('sampling_footprint','var')==0)
     sampling_footprint = 5;
 end
 sampling = setupsample(sampling_footprint,1,1);
  
  %% Run the vehicle
  auv=moveauv(waypoints,guidance,endurance,speed);

  %% Collect samples along the way
  collection=samplemotion(simspace,auv,sampling);
 
  %% Analyse data
  %analysis = analyse_sampling(collection,simspace);
  analysis = NaN;
end