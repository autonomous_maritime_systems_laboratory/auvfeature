% This function is part of the auvfeature simulation collection
% Use this to setup specific scenarios for testing with the simulator
% - this should be turned into a function and controlled by a GUI
% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Setup the collection vectors
% Setup scenario
replicates = 1;
targetsizerange = 100:100:100;
targetnumberrange = 3:1:3;
iterations = replicates * length(targetsizerange) * length(targetnumberrange);
disp(['Running ' num2str(iterations) ' iterations of the AUV sampling'])

% Generate the input vectors
targetnumlist = repmat(targetnumberrange,replicates,1);
targetnumlist = targetnumlist(:);
targetnumlist = repmat(targetnumlist,1,length(targetsizerange));
% Geneate the collection vectors
observedtargets1 = nan((iterations/length(targetsizerange)),length(targetsizerange));
observedtargets2 = nan((iterations/length(targetsizerange)),length(targetsizerange));
observedtargets3 = nan((iterations/length(targetsizerange)),length(targetsizerange));

sizetargets = nan((iterations/length(targetsizerange)),length(targetsizerange));
iterationid = repmat((1:(iterations/length(targetsizerange)))',1,length(targetsizerange));

actualtargets =  nan((iterations/length(targetsizerange)),length(targetsizerange));

targetsize = repmat(targetnumberrange',1,length(targetsizerange));
iterationid = repmat((1:replicates)',(iterations/replicates),1);

%% Set the space and target
east = 2000;
north = 1500;
res = 5;

endurance = 20000;
speed = 1.5;
guidance = 0.5;

%% Generate waypoints
setwaypoints;
for jj=1:length(targetsizerange)
       % I don't like nested for loops, but...
        for ii = 1:(iterations/length(targetsizerange))
        [simspace, retarget] = setup_space_and_target(east,north,res,res,targetsizerange(jj));
   
        numtargets = targetnumlist(ii,jj);
       
        actualtargets(ii,jj) = numtargets;
        sizetargets(ii,jj) = targetsizerange(jj);
    
        %% Run the AUV mission and extract analysis

        % Survey pattern 1
        [simspace,auv1,collection1,analysis1] = runauvsimulation(simspace,retarget,waypoints3,numtargets,endurance,speed,guidance);
        observedtargets1(ii,jj)=analysis1.numobservedtargets;
        
        % Survey pattern 2
        [~,auv2,collection3,analysis2] = runauvsimulation(simspace,retarget,waypoints4,numtargets,endurance,speed,guidance);
        observedtargets2(ii,jj)=analysis2.numobservedtargets;
        
        % Survey pattern 3
        [~,auv3,collection3,analysis3] = runauvsimulation(simspace,retarget,waypoints5,numtargets,endurance,speed,guidance);
        observedtargets3(ii,jj)=analysis3.numobservedtargets;

        end
        
            % Save the data every iteration
           % filename = ['/Users/dguihen/Documents/sampling_temp/sampling_save_size_class_save_' num2str(jj)];
           % save (filename);
          %  progress = 100*(jj/length(targetsizerange));
           % disp([num2str(progress),'% complete'])
end

%% Straighen out all the arrays
observedtargets1 = observedtargets1(:);
observedtargets2 = observedtargets2(:);
observedtargets3 = observedtargets3(:);
actualtargets = actualtargets(:);
sizetargets = sizetargets(:);
iterationid = iterationid(:);
%% Clean up the stray variables
%clear east numtargets endurance filename guidance iterations jj north ii progress replicates res retarget speed targetnumberrange targetnumlist targetsize targetsizerange ans