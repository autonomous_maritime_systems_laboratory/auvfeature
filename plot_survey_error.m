% Get unique target numbers
tnum = unique(actualtargets);

% Get unique size classes
tsize = unique(sizetargets);

% For each class, get stats
meanobserved=nan(length(tnum),length(tsize));
stdobserved=nan(length(tnum),length(tsize));
numvec=nan(length(tnum),length(tsize));
sizevec=nan(length(tnum),length(tsize));
for ii=1:length(tnum)
    for jj=1:length(tsize)
        range1 = observedtargets1(actualtargets == tnum(ii) & sizetargets == tsize(jj));
        range2 = observedtargets2(actualtargets == tnum(ii) & sizetargets == tsize(jj));
        range3 = observedtargets3(actualtargets == tnum(ii) & sizetargets == tsize(jj));
        
        meanobserved1(ii,jj)=nanmean(range1);
        stdobserved1(ii,jj)=nanstd(range1);
        
        meanobserved2(ii,jj)=nanmean(range2);
        stdobserved2(ii,jj)=nanstd(range2);

        meanobserved3(ii,jj)=nanmean(range3);
        stdobserved3(ii,jj)=nanstd(range3);

        numvec(ii,jj)=tnum(ii);
        sizevec(ii,jj)=tsize(jj);
    end
end
numvec = numvec(:);
sizevec = sizevec(:);

%% Make the figure

% figure;
% hold on;
% box on;
% errorbar(tsize,meanobserved1(1,:),stdobserved1(3,:),'b','LineWidth',2,'DisplayName','Survey 1');
% errorbar(tsize,meanobserved2(1,:),stdobserved1(3,:),'r','LineWidth',2,'DisplayName','Survey 2');
% errorbar(tsize,meanobserved3(1,:),stdobserved1(3,:),'g','LineWidth',2,'DisplayName','Survey 3');
% 
% title('1 Target')
% xlabel('Target Radius (m)')
% ylabel('Targets Observed')
% ylim([0 6]);
% xlim([5 105]);
% 
% legend('Survey 1','Survey 2','Survey 3','Location','NorthWest')
% 
% plot (tsize,repmat(1,1,10),'k')
