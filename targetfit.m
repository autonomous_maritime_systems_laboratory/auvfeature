function newtarget = targetfit(space,target)
% usage targetfit = targetfit(space,target)

% This function is part of the auvfeature simulation collection
% This function takes a target and reformats it for addition to the
% simultation space. This component has been seperated from the target
% positioning as it will be quicker to do this once for the target, rather
% than to re-interpolate for each iteration.
% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Generate a new vector based on the sim space resolution
newvecx=target.txvec(1):space.xres:target.txvec(end);
newvecy=target.tyvec(1):space.yres:target.tyvec(end);

%plot (target.txvec,target.tyvec,'b.');
%hold
%plot (newvecx,newvecy,'ro')

%% Interpolate the target for the model grid
newtarget = target;

newtarget.target=interp2(target.txvec,target.tyvec',target.target,newvecx,newvecy');
newtarget.txvec=newvecx;
newtarget.tyvec=newvecy;
newtarget.xlim=[0 max(newvecx)];
newtarget.ylim=[0 max(newvecy)];

