function [simfield, attempts] = targetposition(varargin)
% usage [simfield, attempts] = targetposition(varargin)

% This function is part of the auvfeature simulation collection
% The function takes a target (generated using targetgenerator.m, and sized for space using targetfit, fits to the space using targetfit.m) and
% places it in an array (defined using makespace.m). The output is the new
% array, with a target embedded. 

% # Input parameters
% -target : Set the target
% -space  : Set the space
% -margin : Set the margin around the space where the target will not be
% placed (in m)
% -overlap : Allow for overlapping targets
% -maxattempt : The maximum number of placement attempts before failing
% (default 1000)
% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Parse the inputs
% Set a warning if too few arguments specified 
if (nargin<4)
    error('Too few inputs have been specified for positioning of target.');    
end
% Set some defaults
overlap=0;
margin=0.1;
maxattempt=1000;

for i=1:length(varargin)
    if (ischar(varargin{i})==1) % Only parse if text
     switch lower(varargin{i})
       
       case 'target' % Set the target variable
          target=varargin{i+1}; 
       
       case 'space' % Set the simulation space
          space=varargin{i+1}; 
       
       case 'margin' % Set the target-free margin (default is 0)
          margin=varargin{i+1}; 
       
       case 'overlap' % Allow targets to overlap
          overlap=1;
          
       case 'maxattempt' % Number of placement attempts
          maxattempt=varargin{i+1}; 

     end
    end
end % Stop parsing the inputs
if (margin==0);margin=0.1;end
%% Look at both the target and the space and figure out how mace space cells are required
% Get useable space - x dimension (take target size from margin to get
% maximim start position

% How many space cells are required to fit the target
xreq=length(space.xvector(space.xvector<=target.xlim(2)));
yreq=length(space.yvector(space.yvector<=target.ylim(2)));

xspacemax=space.xlimits(2)-(margin+target.xlim(2));
xspacepos=(space.xvector(space.xvector>=margin & space.xvector<=(xspacemax)));

% If there is no space for the target, show an error
if (isempty(xspacepos)==1)
   error('There is no space in the field for the target (xdim)');  
end

% Get useable space - y dimension, as with x
yspacemax=space.ylimits(2)-(margin+target.ylim(2));
yspacepos=(space.yvector(space.yvector>=margin & space.yvector<=(yspacemax)));

% If there is no space for the target, show an error
if (isempty(yspacepos)==1)
   error('There is no space in the field for the target (ydim)');  
end

%% Candiate positions
accept=0;
iteration=0;
while accept==0&&iteration<maxattempt
    iteration=iteration+1;
    candx=xspacepos(randperm(length(xspacepos),1));
    candy=yspacepos(randperm(length(yspacepos),1));

    targetposx=[candx candx+xreq-1];
    targetposy=[candy candy+yreq-1];
    if (overlap==1)
        accept=1;
    else
        % Check region for existing features - should really avoid using
        % the find command for this...
     
        targindexx1=find(space.xvector==candx);
        targindexx2=find(space.xvector==candx+target.xlim(2));
        targindexy1=find(space.yvector==candy);
        targindexy2=find(space.yvector==candy+target.ylim(2));
        
        crop=space.spacegrid(targindexx1:targindexx2,targindexy1:targindexy2);
        
        if (sum(crop(:))==0)
            accept=1;
        end
        
    end      
end

if (accept==1)
 %   disp (['Target positioning accepted in ',num2str(iteration),' attempts.'])

%% Fit the target (if you can)
   % generate new vectors for the target
    newtargetvecx=(candx:space.xres:candx+target.xlim(2));
    newtargetvecy=(candy:space.yres:candy+target.ylim(2));

    % match the vectors to space indices
    xstart=find(space.xvector==newtargetvecx(1));
    xstop=find(space.xvector==newtargetvecx(end));
    ystart=find(space.yvector==newtargetvecy(1));
    ystop=find(space.yvector==newtargetvecy(end));

    space.spacegrid(xstart:xstop,ystart:ystop)=space.spacegrid(xstart:xstop,ystart:ystop)+target.target;
    simfield=space.spacegrid;
else
    disp (['Positioning failed after ',num2str(iteration),' attempts.'])
    simfield=space.spacegrid;
end
if (accept==0)
    attempts=NaN;
else
    attempts=iteration;
end

end