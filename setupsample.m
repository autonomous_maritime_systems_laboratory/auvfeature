function sampling=setupsample(radius,resolution,interval)
%usage sampling=setupsample(radius,resolution,interval)

% This function is part of the auvfeature simulation collection
% Use this function to make a sample pattern structure. It takes the input
% radius and makes a circle in a mesh of the given radius. The shape can be
% manually edited later. 1 channel is created by default the intention is
% that they will be mapped to the shape, so that the shape can be manually
% edited to add zones for the channels. This might be useful for a
% split-beam or push-broom analysis.

%At this stage there is no componenant of integration time.

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Make the array and a circle within

pattern = 1;
%% Make the structure

sampling = struct(...
    'interval',interval,...
    'channels',1,...
    'pattern',pattern,...
    'radius',radius...
    );