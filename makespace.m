function simspace = makespace(xlength,ylength,xres,yres)
% usage simspace=makespace(lengthx,lengthy,xres,yres)

% This function is part of the auvfeature simulation collection
% This is a simple function to provide a grid and axes variables in a
% format that will remain constant across the simulation.

% # Define the model space (2D)
% -xlength    : Define the model x (east) in metres
% -ylength : Define the model y (north) in metres
% -xres     : Define the model horizontal resolution in metres
% -yres     : Define the model vertical resolution in metres

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Make the vectors
xvector=0:xres:xlength;
yvector=0:yres:ylength;

%% Make a grid
spacegrid = zeros (length(xvector), length(yvector));
xlimits=[0,max(xvector)];
ylimits=[0,max(yvector)];

%% Put it all in a structure
simspace=struct(...
    'xlimits',xlimits,...
    'ylimits',ylimits,...
    'xres',xres,...
    'yres',yres,...
    'xvector',xvector,...
    'yvector',yvector,...
    'spacegrid',spacegrid...
    ); 