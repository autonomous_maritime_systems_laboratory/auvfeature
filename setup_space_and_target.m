function [simspace, retarget] = setup_space_and_target(spacex,spacey,resx,resy,trad)

% This function is a wrapper for the auvfeature function set. Use this to
% arrange setup the space and target to answer specific questions.
% This feature is seperated from runauvsimulation as it is a proceedure that need not be repeated at every iteration of the simulation.

% Inputs:
%   - spacex    spatial extent of domain, east in metres
%   - spacey    spatial extent of domain, north in metres
%   - resx      resolution domain, east in metres
%   - resy      resolution domain, north in metres
%   - trad      target radius in metres

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Make the simulation space and target

    simspace=makespace(spacex,spacey,resx,resy); % Space
    
    simtarget = targetgenerator('trad',trad,'meshsize',100); % Target
    
    retarget = targetfit(simspace,simtarget); % Reformat target