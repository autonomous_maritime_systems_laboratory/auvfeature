function simtarget = targetgenerator(varargin)
% usage target=targetgenerator(varargin)

% This function is part of the auvfeature simulation collection
% This will generate an structure containing an array representing a target and vectors for its dimensions
% The function accepts a variable number of arguments and it is intended
% that these will grow to add complexity to the target as required

% # Target input parameters
% -trad    : Define the target size (radius) in metres
% -meshsize : Define the resolution for target (defaults to 100x100 grid)

% Damien Guihen, UTAS, July 2016 (damien.guihen@utas.edu.au)

%% Parse the input parameters
% Set a warning if no arguments specified 
if (nargin<1)
    error('No inputs have been specified for target definition');    
end
for i=1:length(varargin)
    if (ischar(varargin{i})==1) % Only parse if text
     switch lower(varargin{i})
       
       case 'trad' % Set the target radius (in metres)
          trad=varargin{i+1}; 
       case 'meshsize' % Set the target resolution (in cells)
          meshsize=varargin{i+1}; 

     end
    end
end % Stop parsing the inputs

%% Generate the target array
% Default meshsize unless specified
if ~exist('meshsize','var')
    meshsize=100;
end

[xx, yy] = meshgrid(1:meshsize);
target = +(sqrt((xx-(meshsize/2)).^2+(yy-(meshsize/2)).^2)<=meshsize/2.00075);

%% Generate the target vectors
vec=0:((2*trad)/(meshsize-1)):(2*trad); %simple vector for dimensions of circular target
txvec=vec;
tyvec=vec';

%% Make variables with limits
xlim=[0 max(txvec)];
ylim=[0 max(tyvec)];

%% Put it in a structure
simtarget=struct(...
    'radius',trad,...
    'target',target,...
    'txvec',txvec,...
    'tyvec',tyvec,...
    'xlim',xlim,...
    'ylim',ylim...
    );